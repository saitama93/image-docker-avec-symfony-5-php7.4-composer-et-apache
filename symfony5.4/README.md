## Etape 1 : Création de l'image

A la raince du projet executer cette commande: 
```console
docker-compose up -d
```

## Etape 2 : Créer un nouveau projet symfony

Poru créer un nouveau projet symfony il faut se connecter au conteneur app et installer le projet à l'aide de composer avec la commande suivante: 

```console
docker exec app_container  composer create-project symfony/website-skeleton app ^5.4
```

## Etape 3 : Avoir les droits sur les fichiers en passant la commande

```console
sudo chown -R $USER app
```

## Etape 4 : Consulter l'application

[App](http://localhost:8000/)

[Phpmyadmin](http://localhost:8080/)
User: root sans mot de passe

[Serveur de mail](http://localhost:8081/)

