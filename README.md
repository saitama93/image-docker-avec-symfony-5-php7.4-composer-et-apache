# Environnement PHP Symfony Docker Apache

## Description

Ce dépot à pour but d'être une base pour tout mes futur projets avec le framwork Symfony (versions : 4.4, 5.4 et version actuelle).
    

## Prérequis 

- Avoir docker d'installer ([Télécharger docker](https://docs.docker.com/get-docker/ "Site offciel Docker"))



Ce dépôt regroupe trois dossiers pour 3 environnements différents pour 3 différentes versions de Symfony



- Symfony 4.4

- Symfony 5.4

- Symfony 6



## Etape 1: Clonage du dépôt

Pour mettre en place un environnement veuillez cloner le dépôt.

```console
git clone https://gitlab.com/saitama93/image-docker-avec-symfony-5-php7.4-composer-et-apache.git
```

On obtient une architecture comme ci-dessous

- symfony4.4
- symfony5.4
- symfony6
- .gitignore
- README


## Etape 2 : Choix de la version 

Une fois le dépôt cloné, rendez-vous dans l'un des 3 dossiers via les liens ci-dessous et laissez vous guider.

[Symfony 4.4](https://gitlab.com/saitama93/image-docker-avec-symfony-5-php7.4-composer-et-apache/-/tree/master/symfony4.4)

[Symfony 5.4](https://gitlab.com/saitama93/image-docker-avec-symfony-5-php7.4-composer-et-apache/-/tree/master/symfony5.4)

[Symfony 6](https://gitlab.com/saitama93/image-docker-avec-symfony-5-php7.4-composer-et-apache/-/tree/master/symfony6)

## Crédits



Ce code source est inspiré d'un tutoriel de la chaine YouTube **YoanDev co**



***Lien vers source vidéo (YouTube)***

*********************



<https://www.youtube.com/watch?v=tRI6KFNKfFo>

